import './bootstrap';

import Alpine from 'alpinejs';
import('alpinejs')
import {createApp} from "vue";
import router from "./router";
import App from './components/App.vue'

createApp({
    components:{
            App
        }
    }
).use(router).mount('#app')



